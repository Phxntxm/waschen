import nextcord, sqlite3, os
from nextcord import Interaction
from nextcord.ext import commands
from math import ceil
from typing import Union

def mkpages(iterable:Union[list,str,tuple,dict,set], items:int):
    pages = []
    items = 1 if items <= 0 else items
    for x in iterable:
        page = 0
        appending = True
        while appending:
            try:
                if len(pages[page]) < items:
                    pages[page].append(x)
                    appending = False
                else:
                    page += 1
            except:
                pages.append([x])
                appending = False
    return tuple(pages)

async def doLog(bot, content):
    try:
        globalLog = await bot.client.fetch_channel(int(os.getenv('LOG')))
        await globalLog.send(content=content)
    except:
        pass
    print(content)
    return 0

async def memberListing(interaction, setType:int) -> None:
    # TYPE:
    # Staff
    # Wiki
    # Reddit
    # Bots

    class typeSelect(nextcord.ui.Select):
        def __init__(self):
            options=[
                nextcord.SelectOption(label="Discord", value=0),
                nextcord.SelectOption(label="Wiki", value=1),
                nextcord.SelectOption(label="Reddit", value=2),
                nextcord.SelectOption(label="Bots", value=3)
            ]
            super().__init__(placeholder="Select Group", options=options)

        async def callback(self, interaction):
            await memberListing(interaction, int(self.values[0]))
    
    view = nextcord.ui.View(timeout=600)
    view.add_item(typeSelect())

    embed = ""

    def getStatus(status):
        if status == nextcord.Status.online:
            return "<:online:1051224319545577502>"
        elif status == nextcord.Status.offline:
            return "<:offline:1051224324385820776>"
        elif status == nextcord.Status.idle:
            return "<:idle:1051224316919943179>"
        elif status == nextcord.Status.dnd:
            return "<:do_not_disturb:1051224321621757972>"
    
    if setType == 0:
        embed = nextcord.Embed(title="Discord Staff List", description="Here is the list of discord staff members:", color=0x3366cc)
        embed.add_field(name="Owner", value=f"* <@{interaction.guild.owner.id}> ({interaction.guild.owner.name}) - {getStatus(interaction.guild.owner.status)}", inline=False)
        admins = ""
        for x in interaction.guild.members:
            for y in x.roles:
                if y.id == 630607689327575040:
                    admins += f"* <@{x.id}> ({x.name}) - {getStatus(x.status)}\n"
        admins = "None" if admins == "" else admins
        embed.add_field(name="Admins", value=admins, inline=False)
        mods = ""
        for x in interaction.guild.members:
            for y in x.roles:
                if y.id == 747298143347015810:
                    mods += f"* <@{x.id}> ({x.name}) - {getStatus(x.status)}\n"
        mods = "None" if mods == "" else mods
        embed.add_field(name="Moderators", value=mods, inline=False)
    elif setType == 1:
        embed = nextcord.Embed(title="Wiki Staff List", description="Here is the list of wiki staff members on discord:", color=0x3366cc)
        wiki = ""
        for x in interaction.guild.members:
            for y in x.roles:
                if y.id == 845026102564487229:
                    wiki += f"* <@{x.id}> ({x.name}) - {getStatus(x.status)}\n"
        wiki = "None" if wiki == "" else wiki
        embed.add_field(name="Wiki Admins", value=wiki)
    elif setType == 2:
        embed = nextcord.Embed(title="Reddit Moderators List", description="Here is the list of reddit moderators on discord:", color=0x3366cc)
        reddit = ""
        for x in interaction.guild.members:
            for y in x.roles:
                if y.id == 747297922256994374:
                    reddit += f"* <@{x.id}> ({x.name}) - {getStatus(x.status)}\n"
        reddit = "None" if reddit == "" else reddit
        embed.add_field(name="Reddit Moderators", value=reddit)
    elif setType == 3:
        embed = nextcord.Embed(title="Bots List", description="Here is the list of bots on this discord server:", color=0x3366cc)
        bots = ""
        for x in interaction.guild.members:
            if x.bot:
                bots += f"* <@{x.id}> ({x.name}) - {getStatus(x.status)}\n"
        bots = "None" if bots == "" else bots
        embed.add_field(name="Bots", value=bots)
    embed.set_footer(icon_url=interaction.guild.icon)
    try:
        await interaction.response.edit_message(embed=embed, view=view)
    except:
        await interaction.response.send_message(embed=embed, view=view)

async def getHelp(interaction, bot, page:int) -> None:
    nextPage = nextcord.ui.Button(label=" Next", style=nextcord.ButtonStyle.blurple, emoji="➡️")
    prevPage = nextcord.ui.Button(label=" Previous", style=nextcord.ButtonStyle.blurple, emoji="⬅️")
    view = nextcord.ui.View(timeout=600)
    view.add_item(prevPage)
    view.add_item(nextPage)

    commands = bot.client.get_all_application_commands()
    pagedCommands = mkpages(commands, 6)
    helpContent = ""
    lastPage = ceil(len(commands)/6)

    if page <= 0:
        page = lastPage
    elif page > lastPage:
        page = 1

    async def callbackNext(interaction):
        await getHelp(interaction, bot, page+1)
    async def callbackPrev(interaction):
        await getHelp(interaction, bot, page-1)

    nextPage.callback = callbackNext
    prevPage.callback = callbackPrev

    try:
        for x in pagedCommands[page-1]:
            helpContent += f"- {x.get_mention(guild=None)}\n> {x.description}\n\n"
    except:
        helpContent = "Empty" if helpContent == "" else helpContent
        page = 0

    embed = nextcord.Embed(title="Help", description=helpContent, color=0x3366cc)
    embed.set_footer(text=f"Page {page}/{lastPage}", icon_url=interaction.guild.icon)
    try:
        await interaction.response.edit_message(embed=embed, view=view)
    except:
        await interaction.response.send_message(embed=embed, view=view)

async def getFeeds(interaction, bot, page:int) -> None:
    nextPage = nextcord.ui.Button(label=" Next", style=nextcord.ButtonStyle.blurple, emoji="➡️")
    prevPage = nextcord.ui.Button(label=" Previous", style=nextcord.ButtonStyle.blurple, emoji="⬅️")
    refreshPage = nextcord.ui.Button(label=" Refresh", style=nextcord.ButtonStyle.blurple, emoji="🔄")
    view = nextcord.ui.View(timeout=600)
    view.add_item(prevPage)
    view.add_item(refreshPage)
    view.add_item(nextPage)

    feeds = bot.c.execute("SELECT name FROM feeds").fetchall()

    pagedFeeds = mkpages(feeds, 6)
    feedContent = ""
    lastPage = ceil(len(feeds)/6)

    if page <= 0:
        page = lastPage
    elif page > lastPage:
        page = 1

    async def callbackNext(interaction):
        await getFeeds(interaction, bot, page+1)
    async def callbackRefresh(interaction):
        await getFeeds(interaction, bot, page)
    async def callbackPrev(interaction):
        await getFeeds(interaction, bot, page-1)

    nextPage.callback = callbackNext
    refreshPage.callback = callbackRefresh
    prevPage.callback = callbackPrev

    try:
        for x in pagedFeeds[page-1]:
            channel_id = bot.c.execute("SELECT channel_id FROM feeds WHERE name = ?", (x[0],)).fetchone()[0]
            rss = bot.c.execute("SELECT rss FROM feeds WHERE name = ?", (x[0],)).fetchone()[0]
            feedContent += f"- `{x[0]}` - <#{channel_id}> - [rss-url]({rss})\n\n"
    except:
        feedContent = "No feeds availlable" if feedContent == "" else feedContent
        page = 0

    embed = nextcord.Embed(title="Feeds", description=feedContent, color=0x3366cc)
    embed.set_footer(text=f"Page {page}/{lastPage}", icon_url=interaction.guild.icon)
    try:
        await interaction.response.edit_message(embed=embed, view=view)
    except:
        await interaction.response.send_message(embed=embed, view=view)

async def getPage(interaction, bot, page:int, setType:int) -> None:
    # TYPE:
    # 0 = filters
    # 3 = registered threads

    class typeSelect(nextcord.ui.Select):
        def __init__(self, bot):
            options=[
                nextcord.SelectOption(label="Filter Channels", value=0, emoji="🧹"),
                nextcord.SelectOption(label="Registered Threads", value=3, emoji="🧵")
            ]
            super().__init__(placeholder="Select Type", options=options)
            self.bot = bot

        async def callback(self, interaction):
            await getPage(interaction, self.bot, 1, int(self.values[0]))

    nextPage = nextcord.ui.Button(label=" Next", style=nextcord.ButtonStyle.blurple, emoji="➡️")
    prevPage = nextcord.ui.Button(label=" Previous", style=nextcord.ButtonStyle.blurple, emoji="⬅️")
    refreshPage = nextcord.ui.Button(label=" Refresh", style=nextcord.ButtonStyle.blurple, emoji="🔄")
    view = nextcord.ui.View(timeout=600)
    view.add_item(prevPage)
    view.add_item(refreshPage)
    view.add_item(nextPage)
    view.add_item(typeSelect(bot))

    data = []
    if setType == 0:
        data = bot.c.execute(f"SELECT channel_id FROM channels WHERE guild_id = {interaction.guild.id}").fetchall()
    if setType == 1:
        data = bot.c.execute(f"SELECT channel_id FROM channels WHERE guild_id = {interaction.guild.id}").fetchall()
    elif setType == 3:
        data = bot.c.execute(f"SELECT thread_id FROM threads WHERE guild_id = {interaction.guild.id}").fetchall()

    pagedData = mkpages(data, 6)
    dataContent = ""
    lastPage = ceil(len(data)/6)

    if page <= 0:
        page = lastPage
    elif page > lastPage:
        page = 1

    async def callbackNext(interaction):
        await getPage(interaction, bot, page+1, setType)
    async def callbackRefresh(interaction):
        await getPage(interaction, bot, page, setType)
    async def callbackPrev(interaction):
        await getPage(interaction, bot, page-1, setType)

    nextPage.callback = callbackNext
    refreshPage.callback = callbackRefresh
    prevPage.callback = callbackPrev

    try:
        for x in pagedData[page-1]:
            dataContent += f"- <#{x[0]}>\n\n"
    except:
        dataContent = "Empty" if dataContent == "" else dataContent
        page = 0

    info = f"""
    -Status: {bot.client.status}
    -Latency: {bot.client.latency}
    -User: {bot.client.user.mention}
    -Total registered threads: {len(bot.c.execute(f"SELECT thread_id FROM threads WHERE guild_id = {interaction.guild.id}").fetchall())}"""
    embed = nextcord.Embed(title=f"{bot.client.user.name} Stats", description=info, color=0x3366cc)
    if setType == 0:
        embed.add_field(name="Filter Channels", value=dataContent)
    elif setType == 3:
        embed.add_field(name="Registered Threads", value=dataContent)
    embed.set_footer(text=f"Page {page}/{lastPage}", icon_url=interaction.guild.icon)
    try:
        await interaction.response.edit_message(embed=embed, view=view)
    except:
        await interaction.response.send_message(embed=embed, view=view)

    async def on_error(self, error, item, interaction):
        await doLog(self.bot, f"⚠ Error: `{error}`")
        raise error

class filterModal(nextcord.ui.Modal):
    def __init__(self, bot, channel, edit:bool):
        modalTitle = "Edit Filter Channel" if edit else "Setup Filter Channel"
        super().__init__(modalTitle, auto_defer=True)
        self.channel = channel
        self.bot = bot
        self.edit = edit

        default_name = bot.c.execute(f"SELECT str_val2 FROM channels WHERE channel_id = {channel.id}").fetchone()[0] if edit else ""
        default_warn = bot.c.execute(f"SELECT str_val1 FROM channels WHERE channel_id = {channel.id}").fetchone()[0] if edit else ""

        self.defaultThreadName = nextcord.ui.TextInput(label="Default Thread Name:", min_length=5, max_length=100, default_value=default_name, required=True, style=nextcord.TextInputStyle.short)
        self.add_item(self.defaultThreadName)
        self.warnMsg = nextcord.ui.TextInput(label="Warning Message:", min_length=5, max_length=1900, default_value=default_warn, required=True, style=nextcord.TextInputStyle.paragraph)
        self.add_item(self.warnMsg)

    async def callback(self, interaction:Interaction) -> None:
        if self.edit:
            self.bot.c.execute(f"UPDATE channels SET str_val1 = '{self.warnMsg.value}' WHERE channel_id = {self.channel.id}")
            self.bot.conn.commit()
            self.bot.c.execute(f"UPDATE channels SET str_val2 = '{self.defaultThreadName.value}' WHERE channel_id = {self.channel.id}")
            self.bot.conn.commit()
            await interaction.response.send_message(f"{self.channel.mention}'s filter settings has been updated.")
        else:
            sql = "INSERT INTO channels (channel_id, guild_id, str_val1, str_val2) VALUES (?, ?, ?, ?)"
            val = (self.channel.id, interaction.guild.id, self.warnMsg.value, self.defaultThreadName.value)
            self.bot.c.execute(sql,val)
            self.bot.conn.commit()
            await interaction.response.send_message(f"{self.channel.mention} has been added as filter channel.")
        return 0

    async def on_error(self, error, interaction):
        await doLog(self.bot, f"⚠ Error: `{error}`")
        raise error

class renameModal(nextcord.ui.Modal):
    def __init__(self, bot, thread):
        super().__init__("Rename Thread")
        self.bot = bot
        self.thread = thread

        self.set_name = nextcord.ui.TextInput(label="Thread Name:", max_length=100, required=True, style=nextcord.TextInputStyle.short)
        self.add_item(self.set_name)

    async def callback(self, interaction:Interaction) -> None:
        await self.thread.edit(name=self.set_name.value)
        return 0

    async def on_error(self, error, interaction):
        await doLog(self.bot, f"⚠ Error: `{error}`")
        raise error

class renameThread(nextcord.ui.Button):
    def __init__(self, bot, thread, caller):
        super().__init__(emoji="📝", style=nextcord.ButtonStyle.blurple)
        self.bot = bot
        self.thread = thread
        self.caller = caller

    async def callback(self, interaction:Interaction):
        if self.caller == interaction.user.id:
            await interaction.response.send_modal(renameModal(self.bot, self.thread))
        else:
            await interaction.response.send_message(f"Only <@{self.caller}> may do this.", ephemeral=True)

class threadView(nextcord.ui.View):
    def __init__(self, bot, thread, caller):
        super().__init__(timeout=600)
        self.bot = bot
        self.add_item(renameThread(bot, thread, caller))

    async def on_error(self, error, item, interaction):
        await doLog(self.bot, f"⚠ Error: `{error}`", 0)
        raise error

